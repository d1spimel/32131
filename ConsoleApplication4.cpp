﻿#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    float y, z, n;
    cout << "Enter y: "; cin >> y;
    z = 1;
    for (n = 0; n < 10; n++)
    {
        z *= (pow(y, 3) + 2) / pow(y, 2) + 5;
    }
    cout << "Result: " << z;
}
